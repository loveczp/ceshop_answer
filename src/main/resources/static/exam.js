const ax = axios.create();
const comp_register = {
  data() {
    return {
      name: "",
      password1: "",
      password2: "",
      msgs: [],
      disabled: "disabled",
    };
  },
  computed: {
    isDisabled: function () {
      return this.msgs.length > 0;
    },
  },
  methods: {
    register: function () {
      ax.get("/register", {
        params: { name: this.name, password: this.password1 },
      }).then((response) => {
        if (response.data.message == "ok") {
          alert("registered, go to login now");
          this.$parent.viewname = "login";
        } else {
          alert("error:" + response.data.message);
        }
      });
    },
    check: function () {
      this.msgs = [];
      if (!this.password1 == this.password2) {
        this.msgs.push("password1 should be equal to password2");
      }
      if (this.password1.length < 4) {
        this.msgs.push("password length should be at least 4");
      }
      if (this.name == "") {
        this.msgs.push("name should not be empty");
      }
    },
  },
  template: `
     <div id="register">
        <br>
        <br>
        <br>
        <br>
        <div>Please register with user and password</div>
        <br>
        <br>
        <label>user name:</label><input placeholder="your user name" v-model="name" v-on:change="check"/><br>
        <label>password:</label><input type="password" v-model="password1" v-on:change="check"/><br>
        <label>repeat password:</label><input type="password" v-model="password2" v-on:change="check"/><br>
        <div v-for="msg in msgs"><label style="color:red;">{{msg}}</label></div>
        <input type="button" value="submit" v-on:click="register" :disabled="isDisabled"/>
        <br>
        <br>
        <p>if you already have a user name go to <a v-on:click="$parent.viewname='login'">LOGIN</a></p>
      </div>
    `,
};

const comp_login = {
  data() {
    return {
      name: "",
      password: "",
      msgs: [],
      disabled: true,
    };
  },
  computed: {
    isDisabled: function () {
      return this.msgs.length > 0;
    },
  },
  methods: {
    login: function () {
      ax.get("/login", {
        params: { name: this.name, password: this.password },
      }).then((response) => {
        if (response.data.message == "ok") {
          this.$parent.initexam();
          this.$parent.viewname = "exam";
        } else {
          alert("error:" + response.data.message);
          this.msgs.push(response.data.message);
        }
      });
    },
    check: function () {
      this.msgs = [];
      if (this.password.length == 0) {
        this.msgs.push("please input password");
      }
      if (this.name == "") {
        this.msgs.push("please input name");
      }
    },
  },
  template: `
    <div>
      <div>to take the test, Please LOGIN with user and password</div>
      <br>
      <br>
      <label>user name:</label><input placeholder="your name" v-model="name" v-on:change="check"/><br>
      <label>password:</label><input type="password" v-model="password" v-on:change="check"/><br>
      <div v-for="msg in msgs"><label style="color:red;">{{msg}}</label></div>
      <input type="button" value="login" v-on:click="login" :disabled="isDisabled"/>
      <br>
      <br>
      <br>
      <br>
      <p>if you do not have a user name go to <a v-on:click="$parent.viewname='register'">REGISTER</a></p>
    </div>
      `,
};

const comp_exam = {
  props: ["questions", "answers"],
  data() {
    return {
      current: 1,
      msgs: [],
    };
  },
  methods: {
    submit: function (questionNumber) {
      console.log(questionNumber);
      ax.get("/exam/updateAnswer", {
        params: {
          examid: this.questions[0].examId,
          questionNumber: questionNumber,
          answer: this.answers[questionNumber],
        },
      }).then((response) => {
        if (response.data.message == "ok") {
          console.log("submitted");
          alert("submitted");
        } else {
          alert("error:" + response.data.message);
          this.msgs.push(response.data.message);
        }
      });
    },
  },
  template: `
<div>
  <div v-for="q in questions" v-show="q.questionNumber==current">
    <p>{{q.questionDescription}}</p>
    <br>
    <br>
    <br>

    <p v-for="i in [1,2,3,4,5,6]">
    <label>
      <input type="radio"  :name='q.questionNumber'  :value='i'  v-model='answers[q.questionNumber]'>
      {{q["option"+i]}}</label>
    </p>
    <br>
    <button v-on:click="submit(q.questionNumber)">submit</button><br>
    <br>
    <br>
    <button v-on:click="current=current>1?current-1:1">previous</button>
    <span style="width: 100px;display: inline-block;">                   </span>
    <button v-on:click='current=current<questions.length?current+1:questions.length'>next</button>
  </div>
</div>
      `,
};

Vue.component("comp_login", comp_login);
Vue.component("comp_register", comp_register);
Vue.component("comp_exam", comp_exam);

const app = new Vue({
  data: { questions: [], answers: {}, exam: [], viewname: "login" },
  methods: {
    parentReact: function () {
      console.log("this is a test, react from parent component");
    },
    initexam: function () {
      ax.get("/exam/getExamNAnswer?examid=1", {
        params: { name: this.name, password: this.password },
      }).then((response) => {
        if (response.data.message == "ok") {
          this.exam = response.data.exam;
          this.questions = response.data.exam.questions;
          this.answers = {};
          for (var p in response.data.answers) {
            console.log(p);
            this.answers[response.data.answers[p].questionNumber] =
              response.data.answers[p].answer;
          }
        } else {
          alert("error:" + response.data.message);
          this.msgs.push(response.data.message);
        }
      });
    },
  },
}).$mount("#exam_container");
