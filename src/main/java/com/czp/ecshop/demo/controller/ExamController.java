package com.czp.ecshop.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.czp.ecshop.demo.controller.Exam.Answer;
import com.czp.ecshop.demo.controller.Exam.AnswerDao;
import com.czp.ecshop.demo.controller.Exam.Exam;
import com.czp.ecshop.demo.controller.Exam.ExamDao;
import com.czp.ecshop.demo.controller.Exam.Question;
import com.czp.ecshop.demo.controller.Exam.QuestionDao;
import com.czp.ecshop.demo.controller.Exam.User;
import com.czp.ecshop.demo.controller.Exam.UserDao;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExamController {

    @Autowired
    ExamDao examdao;

    @Autowired
    QuestionDao questiondao;

    @Autowired
    UserDao userdao;

    @Autowired
    AnswerDao answerdao;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public Map<String, String> register(@RequestParam String name, @RequestParam String password) {
        var sha256 = Hashing.sha256().hashBytes(password.getBytes()).toString();
        var user = new User();
        user.name = name;
        user.pwdDigest = sha256;
        try {
            ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("name", GenericPropertyMatchers.exact());
            Example<User> example = Example.of(user, matcher);

            var findresult = userdao.findOne(example);
            if (findresult.isPresent() == true) {
                return ImmutableMap.of("message", "your username is regiestered by others.");
            } else {
                userdao.save(user);
                return ImmutableMap.of("message", "ok");
            }
        } catch (Exception e) {
            return ImmutableMap.of("message", e.getMessage());
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public Map<String, String> login(@RequestParam String name, @RequestParam String password,
            HttpServletRequest request) {
        var sha256 = Hashing.sha256().hashBytes(password.getBytes()).toString();
        var user = new User();
        user.name = name;
        user.pwdDigest = sha256;
        ExampleMatcher matcher = ExampleMatcher.matchingAll();
        Example<User> example = Example.of(user, matcher);
        var findresult = userdao.findOne(example);
        if (findresult.isPresent() == true) {
            request.getSession(true).setAttribute("user", findresult.get());
            return ImmutableMap.of("message", "ok");
        } else {
            return ImmutableMap.of("message", "name and password dones not match.");
        }
    }

    @RequestMapping(value = "/exam/logout", method = RequestMethod.GET)
    public Map<String, String> logout(HttpServletRequest request) {
        request.getSession(true).removeAttribute("user");
        return ImmutableMap.of("message", "ok");
    }

    @RequestMapping(value = "/exam/getExamNAnswer", method = RequestMethod.GET)
    public Map<String, Object> getExamNAnswer(@RequestParam Long examid, HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        var exam = examdao.findById(examid);

        var quest = new Question();
        quest.examId = examid;
        var qus = questiondao.findAll(Example.of(quest, ExampleMatcher.matchingAll()));
        qus.forEach(e -> e.answer = null);
        exam.get().questions = qus;

        var answer = new Answer();
        answer.examId = examid;
        answer.userId = user.id;
        var answers = answerdao.findAll(Example.of(answer, ExampleMatcher.matchingAll()));
        return ImmutableMap.of("message", "ok", "exam", exam.get(), "answers", answers);
    }

    @RequestMapping(value = "/exam/updateAnswer", method = RequestMethod.GET)
    public Map<String, String> updateAnswer(@RequestParam Long examid, @RequestParam Integer questionNumber,
            @RequestParam Integer answer, HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        if (answer > 6 || answer < 1) {
            return ImmutableMap.of("message", "option is wrong, its value should between 1 and 6");
        }
        var ans = new Answer();
        ans.userId = user.id;
        ans.examId = examid;
        ans.questionNumber = questionNumber;

        Example<Answer> ansExample = Example.of(ans, ExampleMatcher.matchingAll());
        var dbans = answerdao.findOne(ansExample);
        if (dbans.isPresent() == true) {
            dbans.get().answer = answer;
            answerdao.save(dbans.get());
        } else {
            ans.answer = answer;
            answerdao.save(ans);
        }
        return ImmutableMap.of("message", "ok");
    }
}
