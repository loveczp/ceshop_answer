package com.czp.ecshop.demo.controller;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Order(1)
@WebFilter(filterName = "authFilter")
@Component
public class AuthFilter implements Filter {

    private static final Set<String> ALLOWED_PATHS = Collections
            .unmodifiableSet(new HashSet<>(Arrays.asList("login", "register")));
    private static final Set<String> Include_PATHS = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("exam")));

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String path = request.getRequestURI();
        var paths = Arrays.stream(path.split("/")).filter(e -> e.length() > 0).collect(Collectors.toList());

        if (paths.size() == 0 || ALLOWED_PATHS.contains(paths.get(0))) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if (Include_PATHS.contains(paths.get(0)) && paths.size() > 1) {
            HttpSession session = request.getSession();
            Object user = session.getAttribute("user");
            if (user == null) {
                response.sendRedirect("/login");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}