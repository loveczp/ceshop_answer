package com.czp.ecshop.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Palindrome {
    @RequestMapping(value = "/palindrome", method = RequestMethod.GET)
    public Boolean palindrome(@RequestParam String word) {
        var pword = new StringBuffer(word);
        var psword = pword.reverse().toString();
        if (psword.equals(word)) {
            return true;
        } else {
            return false;
        }
    }
}
