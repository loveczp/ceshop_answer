package com.czp.ecshop.demo.controller.Exam;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "answer", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "userId", "examId", "questionNumber" }) })

public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column()
    public Long userId;

    @Column()
    public Long examId;

    @Column()
    public Integer questionNumber;

    @Column()
    public Integer answer;

}
