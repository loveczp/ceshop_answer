package com.czp.ecshop.demo.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Combination {
    @RequestMapping(value = "/combination", method = RequestMethod.GET)
    public List<List<Integer>> combination(@RequestParam List<String> numbers) {
        var nnumbers = numbers.stream().map(e -> Integer.valueOf(e)).collect(Collectors.toList());
        Collections.sort(nnumbers);
        return this.compute(nnumbers);
    }

    private List<List<Integer>> compute(List<Integer> numbers) {
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        while (numbers.size() > 0) {
            Integer last = numbers.remove(numbers.size() - 1);
            var temp = ans.stream().map(e -> List.copyOf(e)).collect(Collectors.toList());
            for (var i : ans) {
                var t = new ArrayList<Integer>(i);
                t.add(last);
                temp.add(t);
            }

            temp.add(List.of(last));
            ans = temp;
        }

        var ans2 = new ArrayList<List<Integer>>();
        for (var i : ans) {
            if (i.stream().mapToInt(Integer::intValue).sum() == 10) {
                ans2.add(i);
            }
        }
        return ans2;
    }

    public static void main(String[] arg) {
        var p = new Combination();
        System.out.println(p.compute(new ArrayList(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0))));
    }
}
