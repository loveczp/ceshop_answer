package com.czp.ecshop.demo.controller.Exam;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "question", uniqueConstraints = { @UniqueConstraint(columnNames = { "exam_id", "question_number" }) })
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "exam_id", nullable = false)
    public Long examId;

    @Column(name = "question_number")
    public Integer questionNumber;

    @Column(name = "question_description")
    public String questionDescription;

    @Column()
    public String option1;

    @Column()
    public String option2;

    @Column()
    public String option3;

    @Column()
    public String option4;

    @Column()
    public String option5;

    @Column()
    public String option6;

    @Column()
    public Integer answer;
}
